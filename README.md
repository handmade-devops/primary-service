# handmade-devops Business Logic Microservice

[![Conventional Commits](https://img.shields.io/badge/Conventional%20Commits-grey.svg)](https://conventionalcommits.org)
[![MIT license](https://img.shields.io/badge/License-MIT-blue.svg)](https://lbesson.mit-license.org/)
[![Open Source? Yes!](https://badgen.net/badge/Open%20Source%20%3F/Yes%21/blue?icon=github)](https://github.com/Naereen/badges/)
[![Generic badge](https://img.shields.io/badge/NodeJS-14.x-<COLOR>.svg)](https://shields.io/)
[![pipeline status](https://gitlab.com/handmade-devops/primary-service/badges/master/pipeline.svg)](https://gitlab.com/handmade-devops/primary-service/commits/master)

## How to use

```sh
# Copy example environment variables file
cp .env.example .env
# Customise it, if needed

# Run the docker container
docker-compose up

# You have hot reload using nodemon
# and bind mounts, so no restart is needed on code change
```

## Branch management

- `dev` is the main development branch. Always start from here
- `feat/*` is a feature branch, to be merged back into dev
- `fix/*` is a bugfix branch, to be merged back into dev

## How to release

```sh
# 2. Relase
npm run release
# 3. Push the tags
git push --follow-tags
```
