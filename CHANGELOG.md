# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

## [1.2.0](https://gitlab.com/handmade-devops/primary-service/compare/v1.1.0...v1.2.0) (2021-03-12)


### Features

* :star2: added Docker ([e014731](https://gitlab.com/handmade-devops/primary-service/commit/e0147314bb10409fe2e55b59e580777742aefece))

## [1.1.0](https://gitlab.com/handmade-devops/primary-service/compare/v1.0.1...v1.1.0) (2021-03-12)


### Features

* :star2: added base Express app ([d10e680](https://gitlab.com/handmade-devops/primary-service/commit/d10e68004080abac1c170b4b28127c0421373630))
* :star2: added healtcheck endpoint ([ac1b14a](https://gitlab.com/handmade-devops/primary-service/commit/ac1b14ac12de2f4da4392f1b1628be17e0e3d7ff))
* :star2: added OpenAPI basic config ([26c7e22](https://gitlab.com/handmade-devops/primary-service/commit/26c7e229445be40db003dce626f60599eb963a24))
* :star2: added root router ([7188fe8](https://gitlab.com/handmade-devops/primary-service/commit/7188fe8bfb85e5dbd2256d19e93c65803ac6937e))

### 1.0.1 (2021-03-11)

## 1.0.0 (2021-03-11)
