const Router = require('express').Router()

Router.get('/hello', (_req, res) => {
  res.status(200).json({
    message: 'world',
  })
})

const organisationController = require('./modules/organisation/controller')
Router.use('/organisation', organisationController)

const campaignController = require('./modules/campaign/controller')
Router.use('/campaign', campaignController)

const productController = require('./modules/product/controller')
Router.use('/product', productController)

const basketController = require('./modules/basket/controller')
Router.use('/basket', basketController)

module.exports = Router
