const { default: axios } = require('axios')
const config = require('./config')
const { ServerError } = require('./ServerError')

const authorize = (...roles) => async (req, _res, next) => {
  if (!req.headers.authorization) throw new ServerError('Authorization Header missing!', 403)

  const {
    data: { id, user_role, organisation_id },
  } = await axios.get(`http://${config.AUTH_SERVICE}/api/auth/`, {
    headers: { Authorization: req.headers.authorization },
  })

  if (!roles.includes(user_role)) throw new ServerError(`${user_role} is not allowed for this method!`, 403)

  req.state = { id, organisation_id }
  next()
}

module.exports = {
  authorize,
}
