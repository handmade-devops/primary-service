const logger = require('./logger')

const unknownEndpoint = (_req, res) => {
  res.status(404).send({ error: 'unknown endpoint' })
}

const logAxiosError = (err) => {
  const { url, method, data: configData, headers: configHeaders } = err.config
  const { status, statusText, headers, data } = err.response

  logger.error({
    request: { url, method, data: configData, headers: configHeaders },
    response: { status, statusText, headers, data },
  })
}

const errorHandler = (err, _req, res, _next) => {
  err.response ? logAxiosError(err) : logger.error(err)

  let status = 500
  let message = 'Something Bad Happened'

  if (err.response && err.response.data) {
    status = err.response.status ?? status
    message = err.response.data.message ?? message
  } else if (err.message || err.httpStatus) {
    status = err.httpStatus ?? status
    message = err.message ?? message
  }

  err.errors
    ? res.status(err.status).json({ message: err.message, errors: err.errors }) // openapi errors
    : res.status(status).json({ message }) // regular server errors
}

module.exports = {
  unknownEndpoint,
  errorHandler,
}
