const { default: axios } = require('axios')
const logger = require('./logger')

const getAll = (name, url) => async (req, res) => {
  logger.info(`Getting all ${name}...`)

  const { data } = await axios.get(url, { params: req.query })
  res.json(data)
}

const getOne = (name, url) => async (req, res) => {
  const { id } = req.params
  logger.info(`Getting ${name} with id ${id}`)

  const { data } = await axios.get(`${url}/${id}`)
  res.json(data)
}

module.exports = { getAll, getOne }
