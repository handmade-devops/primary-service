const PORT = process.env.PORT || 8080
const NODE_ENV = process.env.NODE_ENV || 'development'
const AUTH_SERVICE = process.env.AUTH_SERVICE || ''
const DATA_SERVICE = process.env.DATA_SERVICE || ''
const BUYER_ROLE = 'BUYER'
const SELLER_ROLE = 'SELLER'

module.exports = {
  PORT,
  NODE_ENV,
  AUTH_SERVICE,
  DATA_SERVICE,
  BUYER_ROLE,
  SELLER_ROLE,
}
