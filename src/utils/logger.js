const { Logger } = require('tslog')

const logger = new Logger({
  name: 'log',
  printLogMessageInNewLine: true,
  overwriteConsole: true,
})

module.exports = logger
