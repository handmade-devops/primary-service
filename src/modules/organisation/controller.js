const { default: axios } = require('axios')
const Router = require('express').Router
const { authorize } = require('../../utils/authorization')
const { DATA_SERVICE, AUTH_SERVICE, SELLER_ROLE } = require('../../utils/config')
const { getAll, getOne } = require('../../utils/helpers')
const logger = require('../../utils/logger')
const { ServerError } = require('../../utils/ServerError')

const baseUrl = `http://${DATA_SERVICE}/api/organisation`
const router = Router()

router.get('/', getAll('organisations', baseUrl))

router.post('/', authorize(SELLER_ROLE), async (req, res) => {
  const { name, description } = req.body
  const { organisation_id } = req.state
  logger.info(`Adding organisation with name ${name}`)
  if (organisation_id !== null) throw new ServerError(`You are not allowed to create another organisation`, 403)

  const { data: organisation } = await axios.post(`${baseUrl}/`, { name, description })

  await axios.put(
    `http://${AUTH_SERVICE}/api/user`,
    {
      organisation_id: organisation.id,
    },
    { headers: { Authorization: req.headers.authorization } },
  )

  res.status(201).json(organisation)
})

router.get('/:id', getOne('organisation', baseUrl))

router.put('/:id', authorize(SELLER_ROLE), async (req, res) => {
  const { id } = req.params
  const { name, description, logo_path } = req.body
  const { organisation_id } = req.state
  logger.info(`Update organisation with id ${id}`)
  if (organisation_id !== parseInt(id)) throw new ServerError(`You are not allowed to update this organisation`, 403)

  const { data: organisation } = await axios.put(`${baseUrl}/${id}`, { name, description, logo_path })
  res.json(organisation)
})

router.delete('/:id', authorize(SELLER_ROLE), async (req, res) => {
  const { id } = req.params
  const { organisation_id } = req.state
  logger.info(`Deleting organisation with id ${id}`)
  if (organisation_id !== parseInt(id)) throw new ServerError(`You are not allowed to delete this organisation`, 403)

  await axios.put(
    `http://${AUTH_SERVICE}/api/user`,
    {
      organisation_id: null,
    },
    { headers: { Authorization: req.headers.authorization } },
  )

  await axios.delete(`${baseUrl}/${id}`)
  res.status(204).end()
})

module.exports = router
