const { default: axios } = require('axios')
const Router = require('express').Router
const { authorize } = require('../../utils/authorization')
const { DATA_SERVICE, BUYER_ROLE } = require('../../utils/config')
const { getOne } = require('../../utils/helpers')
const logger = require('../../utils/logger')
const { ServerError } = require('../../utils/ServerError')

const baseUrl = `http://${DATA_SERVICE}/api/basket`
const router = Router()

router.get('/', authorize(BUYER_ROLE), async (req, res) => {
  const { id: user_id } = req.state
  logger.info(`Getting all baskets...`)

  const { data: baskets } = await axios.get(`${baseUrl}/user/${user_id}`)
  res.json(baskets)
})

router.post('/', authorize(BUYER_ROLE), async (req, res) => {
  const { product_id, count } = req.body
  const { id: user_id } = req.state
  logger.info(`Adding to basket for user ${user_id}`)

  const { data: basket } = await axios.post(`${baseUrl}/`, { user_id, product_id, count })
  res.status(201).json(basket)
})

router.get('/:id', getOne('basket', baseUrl))

router.put('/:id', authorize(BUYER_ROLE), async (req, res) => {
  const { id } = req.params
  const { count, purchased } = req.body
  const { id: user_id } = req.state
  logger.info(`Update basket with id ${id}`)

  if (purchased === false) throw new ServerError(`You cannot remove a basket transaction`, 400)

  const {
    data: { user_id: uid },
  } = await axios.get(`${baseUrl}/${id}`)
  if (uid !== user_id) throw new ServerError(`You are not allowed to update this basket`, 403)

  const { data: basket } = await axios.put(`${baseUrl}/${id}`, { count, purchased })

  if (basket.purchased)
    await axios.put(`http://${DATA_SERVICE}/api/product/sell/${basket.product_id}`, { count: basket.count })

  res.json(basket)
})

router.delete('/:id', authorize(BUYER_ROLE), async (req, res) => {
  const { id } = req.params
  const { id: user_id } = req.state
  logger.info(`Deleting basket with id ${id}`)

  const {
    data: { user_id: uid },
  } = await axios.get(`${baseUrl}/${id}`)
  if (uid !== user_id) throw new ServerError(`You are not allowed to delete this basket`, 403)

  await axios.delete(`${baseUrl}/${id}`)
  res.status(204).end()
})

module.exports = router
