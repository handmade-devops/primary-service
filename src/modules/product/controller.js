const { default: axios } = require('axios')
const Router = require('express').Router
const { authorize } = require('../../utils/authorization')
const { DATA_SERVICE, SELLER_ROLE } = require('../../utils/config')
const { getAll, getOne } = require('../../utils/helpers')
const logger = require('../../utils/logger')
const { ServerError } = require('../../utils/ServerError')

const baseUrl = `http://${DATA_SERVICE}/api/product`
const campaignUrl = `http://${DATA_SERVICE}/api/campaign`
const router = Router()

router.get('/', getAll('products', baseUrl))

router.get('/campaign/:id', async (req, res) => {
  const { id } = req.params
  logger.info(`Getting all products for campaign ${id}...`)

  const { data } = await axios.get(`${baseUrl}/campaign/${id}`)
  res.json(data)
})

router.post('/', authorize(SELLER_ROLE), async (req, res) => {
  const { name, campaign_id, description, price, total } = req.body
  const { organisation_id } = req.state
  logger.info(`Adding product with name ${name}`)

  const {
    data: { organisation_id: oid },
  } = await axios.get(`${campaignUrl}/${campaign_id}`)
  if (oid !== organisation_id) throw new ServerError(`You are not allowed to create a product in this campaign`, 403)

  const { data: product } = await axios.post(`${baseUrl}/`, { name, campaign_id, description, price, total })
  res.status(201).json(product)
})

router.get('/:id', getOne('product', baseUrl))

router.put('/:id', authorize(SELLER_ROLE), async (req, res) => {
  const { id } = req.params
  const { name, campaign_id, description, price, total } = req.body
  const { organisation_id } = req.state
  logger.info(`Update product with id ${id}`)

  const {
    data: { campaign_id: cid },
  } = await axios.get(`${baseUrl}/${id}`)
  const {
    data: { organisation_id: oid },
  } = await axios.get(`${campaignUrl}/${cid}`)
  if (oid !== organisation_id) throw new ServerError(`You are not allowed to update this product`, 403)

  const { data: product } = await axios.put(`${baseUrl}/${id}`, { name, campaign_id, description, price, total })
  res.json(product)
})

router.delete('/:id', authorize(SELLER_ROLE), async (req, res) => {
  const { id } = req.params
  const { organisation_id } = req.state
  logger.info(`Deleting product with id ${id}`)

  const {
    data: { campaign_id },
  } = await axios.get(`${baseUrl}/${id}`)
  const {
    data: { organisation_id: oid },
  } = await axios.get(`${campaignUrl}/${campaign_id}`)
  if (oid !== organisation_id) throw new ServerError(`You are not allowed to delete this product`, 403)

  await axios.delete(`${baseUrl}/${id}`)
  res.status(204).end()
})

module.exports = router
