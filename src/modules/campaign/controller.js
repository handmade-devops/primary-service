const { default: axios } = require('axios')
const Router = require('express').Router
const { authorize } = require('../../utils/authorization')
const { DATA_SERVICE, SELLER_ROLE } = require('../../utils/config')
const { getAll, getOne } = require('../../utils/helpers')
const logger = require('../../utils/logger')
const { ServerError } = require('../../utils/ServerError')

const baseUrl = `http://${DATA_SERVICE}/api/campaign`
const router = Router()

router.get('/', getAll('campaigns', baseUrl))

router.get('/organisation/:id', async (req, res) => {
  const { id } = req.params
  logger.info(`Getting all campaigns for organisation ${id}...`)

  const { data } = await axios.get(`${baseUrl}/organisation/${id}`)
  res.json(data)
})

router.post('/', authorize(SELLER_ROLE), async (req, res) => {
  const { name, description, start_date, end_date } = req.body
  const { organisation_id } = req.state
  logger.info(`Adding campaign with name ${name}`)

  const { data: campaign } = await axios.post(`${baseUrl}/`, {
    name,
    organisation_id,
    description,
    start_date,
    end_date,
  })
  res.status(201).json(campaign)
})

router.get('/:id', getOne('campaign', baseUrl))

router.put('/:id', authorize(SELLER_ROLE), async (req, res) => {
  const { id } = req.params
  const { name, description, start_date, end_date, cover_path } = req.body
  const { organisation_id } = req.state
  logger.info(`Update campaign with id ${id}`)

  const {
    data: { organisation_id: oid },
  } = await axios.get(`${baseUrl}/${id}`)
  if (oid !== organisation_id) throw new ServerError(`You are not allowed to update this campaign`, 403)

  const { data: campaign } = await axios.put(`${baseUrl}/${id}`, {
    name,
    description,
    start_date,
    end_date,
    cover_path,
  })
  res.json(campaign)
})

router.delete('/:id', authorize(SELLER_ROLE), async (req, res) => {
  const { id } = req.params
  const { organisation_id } = req.state
  logger.info(`Deleting campaign with id ${id}`)

  const {
    data: { organisation_id: oid },
  } = await axios.get(`${baseUrl}/${id}`)
  if (oid !== organisation_id) throw new ServerError(`You are not allowed to delete this campaign`, 403)

  await axios.delete(`${baseUrl}/${id}`)
  res.status(204).end()
})

module.exports = router
